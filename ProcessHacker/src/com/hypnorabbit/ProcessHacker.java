package com.hypnorabbit;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JTabbedPane;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JSlider;

import com.hypnorabbit.handlers.MemoryManager;
import com.hypnorabbit.handlers.WindowHandler;
import com.hypnorabbit.utils.Logger;
import com.sun.jna.platform.win32.WinDef.HWND;

import javax.swing.JTextField;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JMenuBar;
import javax.swing.JMenu;

public class ProcessHacker extends JFrame {

	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTabbedPane tabbedPane;
	private JPanel panel;
	private JLabel lblSelectAProcess;
	private JButton btnNewButton;
	private JTextField textField;
	private JMenuBar menuBar;
	private JMenu mnFile;
	private JLabel lblSearchTimedout;
	private JTextField textField_1;
	private JPanel panel_1;
	private JLabel lblAddressScan;
	private JTextField textField_2;
	private JButton btnSearch;
	private JTextField textField_3;
	private JTextField textField_4;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ProcessHacker frame = new ProcessHacker();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public ProcessHacker() {
		initGUI();
	}

	private void initGUI() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 250, 255);
		setJMenuBar(getMenuBar_1());
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		contentPane.add(getTabbedPane());
	}

	public JTabbedPane getTabbedPane() {
		if (tabbedPane == null) {
			tabbedPane = new JTabbedPane(JTabbedPane.TOP);
			tabbedPane.setBounds(0, 0, 234, 195);
			tabbedPane.addTab("Setup", null, getPanel(), null);
			tabbedPane.addTab("MemScan", null, getPanel_1(), null);
		}
		return tabbedPane;
	}

	public JPanel getPanel() {
		if (panel == null) {
			panel = new JPanel();
			panel.setLayout(null);
			panel.add(getLblSelectAProcess());
			panel.add(getBtnNewButton());
			panel.add(getTextField());
			panel.add(getLblSearchTimedout());
			panel.add(getTextField_1());
		}
		return panel;
	}

	public JLabel getLblSelectAProcess() {
		if (lblSelectAProcess == null) {
			lblSelectAProcess = new JLabel("Enter the window's title:");
			lblSelectAProcess.setFont(new Font("Tahoma", Font.PLAIN, 14));
			lblSelectAProcess.setBounds(10, 11, 153, 22);
		}
		return lblSelectAProcess;
	}

	public void searchForWindow(String title, int timeout) {
		boolean found = false;
		int index = 0;
		while (WindowHandler.findWindow(title) == null) {
			Logger.log(".");
			if (index == timeout) {
				break;
			}
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
			}
			index++;
		}
		if (WindowHandler.findWindow(title) != null) {
			found = true;
		}
		if (found) {
			Logger.logln("\nFound " + title);

			HWND window = WindowHandler.findWindow(title);
			MemoryManager.sethProcess(WindowHandler.getOpenProcess(window));
		} else {
			System.out.print("Process not found, search timed out.");
		}
	}

	public JButton getBtnNewButton() {
		if (btnNewButton == null) {
			btnNewButton = new JButton("Attach to process!");
			btnNewButton.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					String title = getTextField().getText();
					int timeout = Integer.getInteger(getTextField_1().getText());
					searchForWindow(title, timeout);
				}
			});
			btnNewButton.setFont(new Font("Tahoma", Font.PLAIN, 16));
			btnNewButton.setBounds(10, 67, 209, 53);
		}
		return btnNewButton;
	}

	public JTextField getTextField() {
		if (textField == null) {
			textField = new JTextField();
			textField.setBounds(10, 34, 209, 31);
			textField.setColumns(10);
		}
		return textField;
	}

	public JMenuBar getMenuBar_1() {
		if (menuBar == null) {
			menuBar = new JMenuBar();
			menuBar.add(getMnFile());
		}
		return menuBar;
	}

	public JMenu getMnFile() {
		if (mnFile == null) {
			mnFile = new JMenu("File");
		}
		return mnFile;
	}

	public JLabel getLblSearchTimedout() {
		if (lblSearchTimedout == null) {
			lblSearchTimedout = new JLabel("Search Time:");
			lblSearchTimedout.setBounds(10, 134, 77, 14);
		}
		return lblSearchTimedout;
	}

	public JTextField getTextField_1() {
		if (textField_1 == null) {
			textField_1 = new JTextField();
			textField_1.setText("5");
			textField_1.setBounds(88, 131, 25, 20);
			textField_1.setColumns(10);
		}
		return textField_1;
	}

	public JPanel getPanel_1() {
		if (panel_1 == null) {
			panel_1 = new JPanel();
			panel_1.setLayout(null);
			panel_1.add(getLblAddressScan());
			panel_1.add(getTextField_2());
			panel_1.add(getBtnSearch());
			panel_1.add(getTextField_3());
			panel_1.add(getTextField_4());
		}
		return panel_1;
	}

	public JLabel getLblAddressScan() {
		if (lblAddressScan == null) {
			lblAddressScan = new JLabel("Address:");
			lblAddressScan.setBounds(10, 11, 70, 14);
		}
		return lblAddressScan;
	}

	public JTextField getTextField_2() {
		if (textField_2 == null) {
			textField_2 = new JTextField();
			textField_2.setBounds(68, 8, 86, 20);
			textField_2.setColumns(10);
		}
		return textField_2;
	}

	public JButton getBtnSearch() {
		if (btnSearch == null) {
			btnSearch = new JButton("Search");
			btnSearch.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					int address = Integer.getInteger(getTextField_2().getText());
					int size = Integer.getInteger(getTextField_4().getText());
					getTextField_3().setText(Integer.toString(MemoryManager.readMemory(address, size).getInt(0)));
				}
			});
			btnSearch.setBounds(65, 63, 89, 23);
		}
		return btnSearch;
	}

	public JTextField getTextField_3() {
		if (textField_3 == null) {
			textField_3 = new JTextField();
			textField_3.setBounds(10, 36, 209, 20);
			textField_3.setColumns(10);
		}
		return textField_3;
	}

	public JTextField getTextField_4() {
		if (textField_4 == null) {
			textField_4 = new JTextField();
			textField_4.setBounds(164, 8, 55, 20);
			textField_4.setColumns(10);
		}
		return textField_4;
	}
}
