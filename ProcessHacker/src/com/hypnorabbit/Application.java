package com.hypnorabbit;

import com.hypnorabbit.handlers.MemoryManager;
import com.hypnorabbit.handlers.WindowHandler;
import com.hypnorabbit.utils.Logger;
import com.sun.jna.platform.win32.WinDef.HWND;


public class Application {

	public static String searchableGameTitle = "Guild Wars 2";
	public static String readableGameTitle = "Guild Wars 2";

	public static String procID = "2d4";
	
	public static void main(String args[]) throws ClassNotFoundException, InstantiationException, IllegalAccessException {
		Logger.log("Searching for " + readableGameTitle);
		while (WindowHandler.findWindow(searchableGameTitle) == null) {
			Logger.log(".");
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
			}
		}
		Logger.logln("\nFound " + readableGameTitle);

		HWND window = WindowHandler.findWindow(searchableGameTitle);
		MemoryManager.sethProcess(WindowHandler.getOpenProcess(window));
		//Logger.log(Player.getPlayerX());
		//Logger.logln(MemoryManager.readMemory(0x400000 + 0x158927c, 4).getInt(0));
		//Logger.logln("Current character's level: " + Player.getLevel());
		//Logger.log(Camera.getCameraDistance());
	}

}
