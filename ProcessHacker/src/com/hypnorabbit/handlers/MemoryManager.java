package com.hypnorabbit.handlers;

import com.sun.jna.Memory;
import com.sun.jna.Pointer;

public class MemoryManager {
	
	private static Pointer hProcess = null;
	
	public static void sethProcess(Pointer hProcess) {
		MemoryManager.hProcess = hProcess;
	}

	public static Pointer gethProcess() {
		return hProcess;
	}

	public static Memory readMemory(int offset, int size) {
		Memory outputBuffer = new Memory(size);
		WindowHandler.kernel32.ReadProcessMemory(gethProcess(), offset, outputBuffer, size, null);
		return outputBuffer;
	}

	public static void writeMemory(int offset, int value, int size) {
		WindowHandler.kernel32.WriteProcessMemory(gethProcess(), offset, new int[] { value }, size, null);
	}

	public static void writeMemory(int offset, int value[], int size) {
		WindowHandler.kernel32.WriteProcessMemory(gethProcess(), offset, value, size, null);
	}

	public static void writeMemory(int offset, float values[], int size) {
		WindowHandler.kernel32.WriteProcessMemory(gethProcess(), offset, values, size, null);
	}

}
