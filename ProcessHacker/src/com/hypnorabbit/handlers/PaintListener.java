package com.hypnorabbit.handlers;

import java.awt.Graphics;

public interface PaintListener {
	public abstract void onRepaint(Graphics g);
}