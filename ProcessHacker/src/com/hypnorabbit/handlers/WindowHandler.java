package com.hypnorabbit.handlers;

import com.hypnorabbit.win32.Kernel32;
import com.hypnorabbit.win32.User32;
import com.sun.jna.Pointer;
import com.sun.jna.platform.win32.WinDef.HWND;


public class WindowHandler {
	
	private static int ACCESS_FLAGS = 1081;
	public static User32 user32;
	public static Kernel32 kernel32;
	
	static {
		user32 = User32.INSTANCE;
		kernel32 = Kernel32.INSTANCE;
	}
	
	public static HWND findWindow(String windowtitle) {
		return user32.FindWindowA(null, windowtitle);
	}

	public static int getWindowProcessID(HWND hWnd) {
		int processID[] = new int[1];
		user32.GetWindowThreadProcessId(hWnd, processID);
		return processID[0];
	}

	public static Pointer getOpenProcess(HWND hWnd) {
		return kernel32.OpenProcess(ACCESS_FLAGS, false, getWindowProcessID(hWnd));
	}
}
