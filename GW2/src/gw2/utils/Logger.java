package gw2.utils;

public class Logger {
	
	public static void log(String data) {
		System.out.print(data);
	}
	public static void log(Float data) {
		System.out.print(Float.toString(data));
	}
	public static void log(int data) {
		System.out.print(Integer.toString(data));
	}
	
	
	public static void logln() {
		System.out.println();
	}
	public static void logln(Float data) {
		System.out.print(Float.toString(data));
	}
	public static void logln(String data) {
		System.out.println(data);
	}
	public static void logln(int data) {
		System.out.println(Integer.toString(data));
	}
	
}