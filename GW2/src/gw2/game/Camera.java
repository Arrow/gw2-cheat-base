package gw2.game;

import gw2.handlers.MemoryManager;

public class Camera {
	
	private static int CAMERA_DISTANCE_ADDRESS = 0x113d810 + 0x10 + 0x11 + 0x12 + 0x13;
	private static int CAMERA_X_ADDRESS = 0x0;
	private static int CAMERA_Y_ADDRESS = 0x0;
	private static int CAMERA_Z_ADDRESS = 0x0;
	
	public static float getCameraDistance() {
		return MemoryManager.readMemory(Constants.GW2_BASE + CAMERA_DISTANCE_ADDRESS, 4).getFloat(0);
	}
	
	public static float getCameraX() {
		 return MemoryManager.readMemory(Constants.GW2_BASE + CAMERA_X_ADDRESS, 12).getFloat(0);
	}
	
	public static float getCameraY() {
		 return MemoryManager.readMemory(Constants.GW2_BASE + CAMERA_Y_ADDRESS, 12).getFloat(0);
	}
	
	public static float getCameraZ() {
		 return MemoryManager.readMemory(Constants.GW2_BASE + CAMERA_Z_ADDRESS, 12).getFloat(0);
	}
}
