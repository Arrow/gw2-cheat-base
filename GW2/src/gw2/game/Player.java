package gw2.game;

import gw2.handlers.MemoryManager;

public class Player {
	
	private static int LEVEL_ADDRESS = 0x118927C; //Integer
	//private static int X_ADDRESS = 0x1475CE4; //Float
	
	public static int getLevel() {
		return MemoryManager.readMemory(Constants.GW2_BASE + LEVEL_ADDRESS, 4).getInt(0L);
	}
	
	public static float getPlayerX() {
		return 0;//MemoryManager.readMemory(Constants.GW2_BASE + X_ADDRESS, 4).getFloat(0L);
	}
	
	public static float getHealth() {
		return MemoryManager.readMemory(Constants.GW2_BASE + 0x0F7601F8, 10).getFloat(0);
	}
}
