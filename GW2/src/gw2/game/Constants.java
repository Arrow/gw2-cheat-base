package gw2.game;

import gw2.handlers.MemoryManager;

public class Constants {
	
	public static int GW2_BASE = 0x00400000;
	public static int UXTHEME_BASE = 0x70150000;//0x701637C9;
	
	/*
	 * Window Addresses
	 */
	/** Size: 4 Bytes */
	public static int WINDOW_HEIGHT = 0x1246400;
	/** Size: 4 Bytes */
	public static int WINDOW_WIDTH = 0x1246404;
	
	
	/**
	 * Contains the possible values for the game's window resolution mode.<br>
	 * Also has a function to get the current mode.
	 * <br><br>
	 * Thanks to JuJuBoSc. <br>
	 * <a href="http://bit.ly/Q6S7Hk">OwnedCore.com: [GW2] Constant Data (Enums, Structs, Etc)</a>
	 */
	public static class ResolutionMode {
		public static int WINDOWED = 0;
		public static int FULLSCREEN = 1;
		public static int FULLSCREENWINDOWED = 2;
		
		public static int getResolutionMode() {
			return MemoryManager.readMemory(Constants.GW2_BASE + 0x119EF38, 4).getInt(0);
		}
	}
}
