package gw2.game;

import com.sun.jna.platform.win32.WinDef.HWND;

import gw2.handlers.MemoryManager;
import gw2.handlers.WindowHandler;
import static gw2.game.Constants.*;

public class Window {
	
	public static int FRAME_OFFSET_X = 6;
	public static int FRAME_OFFSET_Y = 38;
	private HWND gameWindow;
	public int currentX;
	public int currentY;
	public boolean fullscreen = false;
	
	public Window(HWND gameWindow) {
		this.gameWindow = gameWindow;
	}
	
	public int getResolutionMode() {
		//TODO: Check resolution mode
		return ResolutionMode.WINDOWED;
	}
	
	public int getWidth() {
		return MemoryManager.readMemory(Constants.GW2_BASE + Constants.WINDOW_WIDTH, 4).getInt(0);
	}
	
	public int getHeight() {
		return MemoryManager.readMemory(Constants.GW2_BASE + Constants.WINDOW_HEIGHT, 4).getInt(0);
	}
	
	public boolean movedOnX() {
		int cX = currentX;
		if(cX != getX()) {
			return true;
		}
		return false;
	}
	
	public boolean movedOnY() {
		int cY = currentY;
		if(cY != getY()) {
			return true;
		}
		return false;
	}
	
	/**
	 * Get's the top right X coordinate of the application's window.<br>
	 * Does not change if the game goes into fullscreen mode.
	 * @return The X coordinate of the application's window.
	 */
	public int getX() {
		this.currentX = WindowHandler.getRect(gameWindow)[0];
		return this.currentX;
	}
	
	/**
	 * Get's the top right Y coordinate of the application's window.<br>
	 * Does not change if the game goes into fullscreen mode.
	 * @return The Y coordinate of the application's window.
	 */
	public int getY() {
		this.currentY = WindowHandler.getRect(gameWindow)[1];
		return this.currentY;
	}
	
	/**
	 * Get's the bottom right X coordinate of the application's window.<br>
	 * Does not change if the game goes into fullscreen mode.
	 * @return The X coordinate of the application's window.
	 */
	public int getLowerX() {
		return WindowHandler.getRect(gameWindow)[2];
	}
	
	/**
	 * Get's the bottom right Y coordinate of the application's window.<br>
	 * Does not change if the game goes into fullscreen mode.
	 * @return The Y coordinate of the application's window.
	 */
	public int getLowerY() {
		return WindowHandler.getRect(gameWindow)[3];
	}
}
