package gw2.handlers;

import java.awt.Graphics;

public interface PaintListener {
	
	public abstract void onRepaint(Graphics g);
}