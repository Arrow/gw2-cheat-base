package gw2.handlers;

import com.sun.jna.Native;
import com.sun.jna.Pointer;
import com.sun.jna.platform.win32.WinDef.HWND;

import gw2.win32.Kernel32;
import gw2.win32.User32;

public class WindowHandler {
	
	private static int ACCESS_FLAGS = 1081;
	public static User32 user32;
	public static Kernel32 kernel32;
	
	static {
		user32 = User32.INSTANCE;
		kernel32 = Kernel32.INSTANCE;
	}
	
	public static int[] getRect(HWND hWnd) {
		int[] rect = {0, 0, 0, 0};
		user32.INSTANCE.GetWindowRect(hWnd, rect);
		
		return rect;
	}
	
	public static String getWindowTitle(HWND handle) {
		
		return "";
	}
	
	public static HWND findWindow(String windowtitle) {
		return user32.FindWindowA(null, windowtitle);
	}

	public static int getWindowProcessID(HWND hWnd) {
		int processID[] = new int[1];
		user32.GetWindowThreadProcessId(hWnd, processID);
		return processID[0];
	}

	public static Pointer getOpenProcess(HWND hWnd) {
		return kernel32.OpenProcess(ACCESS_FLAGS, false, getWindowProcessID(hWnd));
	}
	
}
