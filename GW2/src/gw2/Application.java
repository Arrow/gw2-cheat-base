package gw2;

import java.util.Timer;

import com.sun.jna.platform.win32.WinDef.HWND;

import gw2.client.GameOverlay;
import gw2.client.GraphicsLoop;
import gw2.client.InputManagerLoop;
import gw2.client.UpdateLoop;
import gw2.game.Constants;
import gw2.game.Player;
import gw2.game.Window;
import gw2.handlers.MemoryManager;
import gw2.handlers.WindowHandler;
import gw2.utils.Logger;

public class Application {

	public static String searchableGameTitle = "Guild Wars 2";
	public static String readableGameTitle = "Guild Wars 2";
	
	public static HWND windowHandle;
	public static Window gameWindow;
	public static GameOverlay gameOverlay;
	
	public static void main(String args[]) throws ClassNotFoundException, InstantiationException, IllegalAccessException {
		Logger.log("Searching for " + readableGameTitle);
		while (WindowHandler.findWindow(searchableGameTitle) == null) {
			Logger.log(".");
			try {
				Thread.sleep(2000L);
			} catch (InterruptedException e) {
			}
		}
		Logger.logln("\nFound " + readableGameTitle);

		windowHandle = WindowHandler.findWindow(searchableGameTitle);
		MemoryManager.sethProcess(WindowHandler.getOpenProcess(windowHandle));
		gameWindow = new Window(windowHandle);
		
		
		gameOverlay = new GameOverlay(gameWindow.getX(), gameWindow.getY());
		
		Timer timer = new Timer();
		timer.schedule(new UpdateLoop(), 0L, 1L);
		timer.schedule(new GraphicsLoop(), 0L, 1L);
		//timer.schedule(new InputManagerLoop(), 0L, 1L);
		
		//System.exit(0);
	}

}
