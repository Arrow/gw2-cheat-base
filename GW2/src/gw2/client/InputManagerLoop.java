package gw2.client;

import gw2.client.input.Mouse;

import java.util.TimerTask;

public class InputManagerLoop extends TimerTask {
	
	private Mouse mouse = null;
	private int index = 0;
	
	public InputManagerLoop() {
		this.mouse = new Mouse();
	}
	
	@Override
	public void run() {
		
		while(index != 20) {
			this.mouse.moveMouse(index, index);
			index++;
		}
	}

}
