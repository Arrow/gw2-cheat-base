package gw2.client;

import static gw2.Application.*;
import gw2.game.Constants;
import gw2.handlers.MemoryManager;
import gw2.handlers.PaintListener;
import gw2.handlers.WindowHandler;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.GraphicsConfiguration;
import java.util.TimerTask;
import javax.swing.JWindow;

import com.sun.jna.*;
import com.sun.jna.examples.WindowUtils;

public class GraphicsLoop extends TimerTask implements PaintListener {

	private float prevX;
	private float prevY;
	private float prevZ;
	private int POS_ADDRESS = 0x059716D0; // The pointed value changes based on the area you are in

	public GraphicsLoop() {
		GraphicsConfiguration gconfig = WindowUtils.getAlphaCompatibleGraphicsConfiguration();
		gameOverlay.alphaWindow = new JWindow(gameOverlay.frame, gconfig);
		GameOverlay.register(this);

		int[] offsets = { 0x44, 0x1c, 0x8c, 0x18, 0x30 };

		Memory pointer = MemoryManager.readMemory(Constants.GW2_BASE + 0x011D3C28, 8);
		//System.out.println();
		Memory pointer2 = MemoryManager.readMemory((int) pointer.getLong(0) + 0x44, 8);//pointer.getLong(0) pointer.getPointer(0x44);
		Memory pointer3 = MemoryManager.readMemory((int) pointer2.getLong(0) + 0x1c, 8);
		Memory pointer4 = MemoryManager.readMemory((int) pointer3.getLong(0) + 0x8c, 8);
		Memory pointer5 = MemoryManager.readMemory((int) pointer4.getLong(0) + 0x18, 8);
		Memory pointer6 = MemoryManager.readMemory((int) pointer5.getLong(0) + 0x30, 8);
		Memory val = MemoryManager.readMemory((int) pointer6.getLong(0), 8);
		//System.out.println(MemoryManager.readMemory((int) val.getLong(0), 8).getFloat(0));

		int start = MemoryManager.readPointer(Constants.GW2_BASE + 0x011D3C28, offsets, 8).getInt(0);

		int value = ((((((0x011D3C28) + 0x44) + 0x1c) + 0x8c) + 0x18) + 0x30);
		System.out.println(MemoryManager.readMemory(value, 8).getInt(0));

		//int pointer2 = MemoryManager.readMemory((int) pointer.getLong(0) + 0x44, 8);//pointer.getLong(0) pointer.getPointer(0x44);
		//int pointer3 = MemoryManager.readMemory((int) pointer2.getLong(0) + 0x1c, 8);
		//int pointer4 = MemoryManager.readMemory((int) pointer3.getLong(0) + 0x8c, 8);
		//int pointer5 = MemoryManager.readMemory((int) pointer4.getLong(0) + 0x18, 8);
		//int pointer6 = MemoryManager.readMemory((int) pointer5.getLong(0) + 0x30, 8);

		this.prevX = MemoryManager.readMemory(POS_ADDRESS, 4).getFloat(0);
		this.prevY = MemoryManager.readMemory(POS_ADDRESS + 0x08, 4).getFloat(0);
		this.prevZ = MemoryManager.readMemory(POS_ADDRESS + 0x04, 4).getFloat(0);
	}

	@Override
	public void run() {
		int topWindowID = WindowHandler.getWindowProcessID(WindowHandler.user32.GetForegroundWindow());
		if (topWindowID == WindowHandler.getWindowProcessID(windowHandle)) {
			topWindowID = -1;
			gameOverlay.update();
			gameOverlay.alphaWindow.setAlwaysOnTop(true);
			// System.out.println("Updating");
		} else {
			gameOverlay.alphaWindow.setAlwaysOnTop(false);
			// gameOverlay.disposeOverlay();
		}
	}

	@Override
	public void onRepaint(Graphics g) {
		g.setColor(Color.GREEN);
		// g.drawString("Hello World!", gameOverlay.getWindowOffsetX(false) + 0,
		// gameOverlay.getWindowOffsetY(false) + 0);

		float newX = MemoryManager.readMemory(POS_ADDRESS, 4).getFloat(0);
		float newY = MemoryManager.readMemory(POS_ADDRESS + 0x08, 4).getFloat(0);
		float newZ = MemoryManager.readMemory(POS_ADDRESS + 0x04, 4).getFloat(0);
		if (this.prevX != newX || this.prevY != newY || this.prevZ != newZ) {

			g.drawString("Window Width: " + gameWindow.getWidth(), gameOverlay.getWindowOffsetX(false) + gameWindow.getWidth() / 2, gameOverlay.getWindowOffsetY(false) + 60);
			g.drawString("Window Height: " + gameWindow.getHeight(), gameOverlay.getWindowOffsetX(false) + gameWindow.getWidth() / 2, gameOverlay.getWindowOffsetY(false) + 70);
			g.drawString("Window X Position: " + gameOverlay.getWindowOffsetX(false), gameOverlay.getWindowOffsetX(false) + gameWindow.getWidth() / 2, gameOverlay.getWindowOffsetY(false) + 80);
			g.drawString("Window Y Position: " + gameOverlay.getWindowOffsetY(false), gameOverlay.getWindowOffsetX(false) + gameWindow.getWidth() / 2, gameOverlay.getWindowOffsetY(false) + 90);

			g.drawString("X: " + newX, gameOverlay.getWindowOffsetX(false) + gameWindow.getHeight() / 2 + 20, gameOverlay.getWindowOffsetY(false) + gameWindow.getWidth() / 2 + 0);
			g.drawString("Y: " + newY, gameOverlay.getWindowOffsetX(false) + gameWindow.getHeight() / 2 + 20, gameOverlay.getWindowOffsetY(false) + gameWindow.getWidth() / 2 + 10);
			g.drawString("Z: " + newZ, gameOverlay.getWindowOffsetX(false) + gameWindow.getHeight() / 2 + 20, gameOverlay.getWindowOffsetY(false) + gameWindow.getWidth() / 2 + 20);
		}
	}
}
