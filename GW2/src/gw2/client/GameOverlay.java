package gw2.client;

import static gw2.game.Constants.*;
import gw2.handlers.PaintListener;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Toolkit;
import java.awt.Window;
import java.awt.image.BufferedImage;
import java.awt.image.Raster;
import java.util.Iterator;
import java.util.LinkedList;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JWindow;
import javax.swing.border.EmptyBorder;

import com.sun.jna.Native;
import com.sun.jna.Pointer;
import com.sun.jna.examples.win32.GDI32;
import com.sun.jna.examples.win32.GDI32.BITMAPINFO;
import com.sun.jna.examples.win32.User32;
import com.sun.jna.examples.win32.User32.BLENDFUNCTION;
import com.sun.jna.examples.win32.User32.POINT;
import com.sun.jna.examples.win32.User32.SIZE;
import com.sun.jna.examples.win32.W32API.HANDLE;
import com.sun.jna.examples.win32.W32API.HBITMAP;
import com.sun.jna.examples.win32.W32API.HDC;
import com.sun.jna.examples.win32.W32API.HWND;
import com.sun.jna.platform.win32.WinGDI;
import com.sun.jna.platform.win32.WinUser;
import com.sun.jna.ptr.PointerByReference;

public class GameOverlay {
	
	public JFrame frame;
	public JWindow alphaWindow;
	private float alpha = 1f;
	private int windowOffsetX = 0;
	private int windowOffsetY = 0;
	
	public Graphics g;
	public int w = 0;
	public int h = 0;
	public BITMAPINFO bmi = null;
	
	public boolean doUpdate;
	
	public int getWindowOffsetX(boolean forceRaw) {
		if (forceRaw) {
			return gw2.game.Window.FRAME_OFFSET_X;
		} else if (ResolutionMode.getResolutionMode() == ResolutionMode.WINDOWED) {
			return gw2.game.Window.FRAME_OFFSET_X + this.windowOffsetX;
		} else {
			return this.windowOffsetX;
		}
	}
	
	public int getWindowOffsetY(boolean forceRaw) {
		if (forceRaw) {
			return gw2.game.Window.FRAME_OFFSET_Y;
		} else if (ResolutionMode.getResolutionMode() == ResolutionMode.WINDOWED) {
			return gw2.game.Window.FRAME_OFFSET_Y + this.windowOffsetY;
		} else {
			return this.windowOffsetY;
		}
	}
	
	
	public static java.util.List<PaintListener> listeners = new LinkedList<PaintListener>();
    public static boolean register(PaintListener pListener) {
        return listeners.add(pListener);
    }
    
	public GameOverlay(int wOffsetX, int wOffsetY) {
		Dimension resolution = Toolkit.getDefaultToolkit().getScreenSize();
		w = (int) resolution.getWidth();
		h = (int) resolution.getHeight();
		this.windowOffsetX = wOffsetX;
		this.windowOffsetY = wOffsetY;
		this.doUpdate = true;
	}
	
	public void updateOffset(int x, int y) {
		this.windowOffsetX = x;
		this.windowOffsetY = y;
		this.update();
	}
	
	public void update() {
		if(this.doUpdate) {
			updateW32();
		}
	}

	private HWND getHwnd(Window w) {
		HWND hwnd = new HWND();
		hwnd.setPointer(Native.getWindowPointer(w));
		return hwnd;
	}
	
	private void updateW32() {
		
		User32 user = User32.INSTANCE;
		GDI32 gdi = GDI32.INSTANCE;
		HWND hWnd = null;
		
		if (!alphaWindow.isDisplayable()) {
			alphaWindow.pack();
			alphaWindow.setAlwaysOnTop(true);
			alphaWindow.setEnabled(false);
			hWnd = getHwnd(alphaWindow);
			int flags = user.GetWindowLong(hWnd, WinUser.GWL_EXSTYLE);
			flags |= WinUser.WS_EX_LAYERED;
			user.SetWindowLong(hWnd, WinUser.GWL_EXSTYLE, flags);
		} else {
			hWnd = getHwnd(alphaWindow);
		}
		HDC screenDC = user.GetDC(null);
		HDC memDC = gdi.CreateCompatibleDC(screenDC);
		HBITMAP hBitmap = null;
		HANDLE oldBitmap = null;

		try {
			BufferedImage buf = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB_PRE);
			this.g = buf.getGraphics();
			
			//this.g = g;
	        for(Iterator<PaintListener> iterator = listeners.iterator(); iterator.hasNext();) {
	            PaintListener l = (PaintListener)iterator.next();
	            if(l instanceof PaintListener)
	                l.onRepaint(g);
	        }
			//g.setColor(Color.BLUE);
			//g.drawString("TEST", this.windowOffsetX + 100, this.windowOffsetY + 100);
			//g.drawOval(this.windowOffsetX + 10, this.windowOffsetY + 10, 20, 50);
			if (bmi == null) {
				bmi = new BITMAPINFO();
				bmi.bmiHeader.biWidth = w;
				bmi.bmiHeader.biHeight = h;
				bmi.bmiHeader.biPlanes = 1;
				bmi.bmiHeader.biBitCount = 32;
				bmi.bmiHeader.biCompression = WinGDI.BI_RGB;
				bmi.bmiHeader.biSizeImage = w * h * 4;
			}
			PointerByReference ppbits = new PointerByReference();
			hBitmap = gdi.CreateDIBSection(memDC, bmi, WinGDI.DIB_RGB_COLORS,
					ppbits, null, 0);
			oldBitmap = gdi.SelectObject(memDC, hBitmap);
			Pointer pbits = ppbits.getValue();

			Raster raster = buf.getData();
			int[] pixel = new int[4];
			int[] bits = new int[w * h];
			for (int y = 0; y < h; y++) {
				for (int x = 0; x < w; x++) {
					raster.getPixel(x, h - y - 1, pixel);
					int alpha = (pixel[3] & 0xFF) << 24;
					int red = (pixel[2] & 0xFF);
					int green = (pixel[1] & 0xFF) << 8;
					int blue = (pixel[0] & 0xFF) << 16;
					bits[x + y * w] = alpha | red | green | blue;
				}
			}
			pbits.write(0, bits, 0, bits.length);

			SIZE size = new SIZE();
			size.cx = w;
			size.cy = h;
			POINT loc = new POINT();
			loc.x = alphaWindow.getX();
			loc.y = alphaWindow.getY();
			POINT srcLoc = new POINT();
			BLENDFUNCTION blend = new BLENDFUNCTION();
			blend.SourceConstantAlpha = (byte) (alpha * 255);
			blend.AlphaFormat = WinUser.AC_SRC_ALPHA;
			user.UpdateLayeredWindow(hWnd, screenDC, loc, size, memDC, srcLoc, 0, blend, WinUser.ULW_ALPHA);
		} finally {
			user.ReleaseDC(null, screenDC);
			if (hBitmap != null) {
				gdi.SelectObject(memDC, oldBitmap);
				gdi.DeleteObject(hBitmap);
			}
			gdi.DeleteDC(memDC);
		}

		if (!alphaWindow.isVisible()) {
			alphaWindow.setVisible(true);
		}
	}
	
}