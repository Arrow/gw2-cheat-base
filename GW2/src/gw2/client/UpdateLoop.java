package gw2.client;

import static gw2.Application.*;

import gw2.game.Constants;

import java.util.TimerTask;

/**
 * This thread updates the API among other things that
 * rely on update info about the game and game's state.
 * 
 * @author Arrow
 */
public class UpdateLoop extends TimerTask {
	
	private int resMode;
	private float prevX;
	private float prevY;
	private float prevZ;
	
	public UpdateLoop() {
		this.resMode = Constants.ResolutionMode.getResolutionMode();
	}
	
	@Override
	public void run() {
		int cX = gameWindow.currentX;
		int cY = gameWindow.currentY;
		if(gameWindow.getX() != cX || gameWindow.getY() != cY) {
			gameOverlay.updateOffset(gameWindow.getX(), gameWindow.getY());
		}
		int resModeN = Constants.ResolutionMode.getResolutionMode();
		if(this.resMode != resModeN) {
			this.resMode = resModeN;
			System.out.println("Resolution Mode changed to: " + resModeN);
		}
		

		
	}

}
