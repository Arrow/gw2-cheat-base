package gw2.win32;

import com.sun.jna.Library;
import com.sun.jna.Native;
import com.sun.jna.Platform;
import com.sun.jna.Pointer;
import com.sun.jna.platform.win32.WinDef.HDC;

public interface GDI32 extends Library {

	public abstract Pointer CreateCompatibleDC(Pointer pointer);

	public abstract Pointer CreateCompatibleBitmap(Pointer pointer, int i, int j);

	public abstract Pointer SelectObject(Pointer pointer, Pointer pointer1);

	public abstract boolean DeleteDC(Pointer pointer);

	public abstract int GetPixel(Pointer pointer, int i, int j);

	public abstract boolean DeleteObject(Pointer pointer);

	public abstract boolean TextOutA(HDC hdc, int i, int j, String s, int k);

	public abstract boolean Rectangle(HDC hdc, int i, int j, int k, int l);

	public abstract int SetBkMode(HDC hdc, int i);

	public abstract int SetTextColor(HDC hdc, int i);

	public static final GDI32 INSTANCE = (GDI32) Native.loadLibrary(Platform.isWindows() ? "gdi32" : null, GDI32.class);

}
