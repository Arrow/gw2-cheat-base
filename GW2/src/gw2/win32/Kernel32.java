package gw2.win32;

import com.sun.jna.Native;
import com.sun.jna.NativeLong;
import com.sun.jna.Pointer;
import com.sun.jna.Structure;
import com.sun.jna.ptr.IntByReference;
import com.sun.jna.win32.StdCallLibrary;

public interface Kernel32 extends StdCallLibrary {
	
	public static Kernel32 INSTANCE = (Kernel32) Native.loadLibrary("Kernel32", Kernel32.class);
	public static final int MAX_PATH = 256;
	public abstract int GetCurrentProcessId();
	public abstract Pointer GetCurrentProcess();
	public abstract int GetLastError();
	public abstract Pointer OpenProcess(int i, boolean flag, int j);
	public abstract int OpenProcess(int i, int j, int k);
	public abstract boolean ReadProcessMemory(Pointer pointer, int i, int ai[], int j, IntByReference intbyreference);
	public abstract boolean ReadProcessMemory(Pointer pointer, int i, Pointer pointer1, int j, IntByReference intbyreference);
	public abstract boolean WriteProcessMemory(Pointer pointer, int i, int ai[], int j, int ai1[]);
	public abstract boolean WriteProcessMemory(Pointer pointer, int i, float af[], int j, int ai[]);
	public abstract boolean CloseHandle(Pointer pointer);
	public abstract Pointer CreateToolhelp32Snapshot(int i, int j);
	public abstract boolean Process32First(Pointer pointer, LPPROCESSENTRY32 lpprocessentry32);
	public abstract boolean Process32Next(Pointer pointer, LPPROCESSENTRY32 lpprocessentry32);
	public abstract Pointer FindResourceA(Pointer pointer, String s, String s1);
	public abstract Pointer LockResource(Pointer pointer);
	public abstract int SizeofResource(Pointer pointer, Pointer pointer1);
	public abstract boolean EnumResourceNamesA(Pointer pointer, String s, ENUMRESNAMEPROC enumresnameproc, IntByReference intbyreference);
	public abstract void GetSystemInfo(LPSYSTEM_INFO lpsystem_info);
	public abstract int VirtualQueryEx(Pointer pointer, Pointer pointer1, MEMORY_BASIC_INFORMATION memory_basic_information, int i);
	
	public static interface ENUMRESNAMEPROC extends StdCallCallback {
		public abstract boolean callback(Pointer pointer, String s, String s1, IntByReference intbyreference);
	}

	public static class LPPROCESSENTRY32 extends Structure {
		public int dwSize;
		public int cntUsage;
		public int th32ProcessID;
		public IntByReference th32DefaultHeapID;
		public int th32ModuleID;
		public int cntThreads;
		public int th32ParentProcessID;
		public NativeLong pcPriClassBase;
		public int dwFlags;
		public char szExeFile[];
		
		public String getSzExeFile() {
			StringBuilder sb = new StringBuilder();
			for (int i = 0; i < szExeFile.length; i++) {
				char a = (char) (szExeFile[i] & 0xff);
				if (a == 0)
					break;
				sb.append(a);
				char b = (char) (szExeFile[i] >> 8 & 0xff);
				if (b == 0)
					break;
				sb.append(b);
			}
			return sb.toString();
		}

		public LPPROCESSENTRY32() {
			szExeFile = new char[256];
			dwSize = size();
		}
	}

	public static class LPSYSTEM_INFO extends Structure {

		public short wProcessorArchitecture;
		public short wReserved;
		public int dwPageSize;
		public Pointer lpMinimumApplicationAddress;
		public Pointer lpMaximumApplicationAddress;
		public Pointer dwActiveProcessorMask;
		public int dwNumberOfProcessors;
		public int dwProcessorType;
		public int dwAllocationGranularity;
		public short wProcessorLevel;
		public short wProcessorRevision;

		//public LPSYSTEM_INFO() {}
	}

	public static class MEMORY_BASIC_INFORMATION extends Structure {

		public Pointer BaseAddress;
		public Pointer AllocationBase;
		public int AllocationProtect;
		public int RegionSize;
		public int State;
		public int Protect;
		public int Type;

		//public MEMORY_BASIC_INFORMATION() {}
	}

}
